В проекте использованы структурные паттерны проектирования Фасад и Декоратор.

Описание классов:
IComponent - интерфейс компонентов декоратора
BaseGraphic - базовый класс декоратора 
BackGround - класс фонового изображения
Frame - класс создания рамки для картинки
PictureWithBackGround - класс добавления картинки на фоновый слой
Title - класс добавления текста мотиватора
Motivator - класс фасада, создающий мотиватор