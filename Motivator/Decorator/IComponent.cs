﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motivator
{
    public interface IComponent
    {
        Graphics GraphicMain { get; set; }
        Image ResultImage { get; set; }
        int WidthImage { get; set; }
        int HeightImage { get; set; }
        void Create();
    }
}
