﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motivator
{
    public class BaseGraphic : IComponent
    {
        protected IComponent nextComponent;
        public Graphics GraphicMain { get; set; }
        public Image ResultImage { get; set; }
        public int WidthImage { get; set; }
        public int HeightImage { get; set; }
        public BaseGraphic(IComponent _nextComponent)
        {
            nextComponent = _nextComponent;
            WidthImage = _nextComponent.WidthImage;
            HeightImage = _nextComponent.HeightImage;
            GraphicMain = _nextComponent.GraphicMain;
            ResultImage = _nextComponent.ResultImage;
        }
        public virtual void Create()
        {          
            nextComponent.Create();
        }
    }
}
