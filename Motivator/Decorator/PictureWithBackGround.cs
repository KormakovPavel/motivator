﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motivator
{
    class PictureWithBackGround : BaseGraphic
    {        
        private readonly Image picture;
        public PictureWithBackGround(Image _picture, IComponent component) : base(component)
        {           
            picture = _picture;
        }
        
        public override void Create()
        {
            base.Create();            
            GraphicMain.DrawImage(picture, WidthImage / 2 - picture.Width/2, HeightImage / 2 - picture.Height/2, picture.Width, picture.Height);            
        }        
    }
}
