﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motivator
{
    class Title : BaseGraphic
    {
        private readonly string textTitle;        
        
        public Title(string text, IComponent component) : base(component)
        {
            textTitle = text;
        }

        public override void Create()
        {
            base.Create();
            GraphicMain.DrawString(textTitle, new Font("Arial", 14), new SolidBrush(Color.White),WidthImage/2 - textTitle.Length*4, HeightImage - 100);
        }
    }
}
