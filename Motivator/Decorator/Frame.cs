﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motivator
{
    class Frame : BaseGraphic
    {
        private readonly int widthFrame;        
        public Frame(int width,IComponent component) : base(component)
        {
            widthFrame = width;            
        }

        public override void Create()
        {
            base.Create(); 
            Point[] points =  { 
                new Point(WidthImage / 2 - 200, HeightImage / 2 - 200), 
                new Point(WidthImage / 2 + 200, HeightImage / 2 - 200), 
                new Point(WidthImage / 2 + 200, HeightImage / 2 + 200), 
                new Point(WidthImage / 2 - 200, HeightImage / 2 + 200), 
                new Point(WidthImage / 2 - 200, HeightImage / 2 - 210) 
            };            
            GraphicMain.DrawLines(new Pen(Color.White, widthFrame),points);            
        }
    }
}
