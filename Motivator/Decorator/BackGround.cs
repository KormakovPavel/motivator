﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motivator
{
    public class BackGround : IComponent
    {
        public Graphics GraphicMain { get; set; }
        public Image ResultImage { get; set; }
        public int WidthImage { get; set; }
        public int HeightImage { get; set; }
        public BackGround(int width, int height)
        {
            WidthImage = width;
            HeightImage = height;

            ResultImage = new Bitmap(WidthImage, HeightImage);

            for (int i = 0; i < WidthImage; i++)
            {
                for (int j = 0; j < HeightImage; j++)
                {
                    ((Bitmap)ResultImage).SetPixel(i, j, Color.Black);
                }
            }

            GraphicMain = Graphics.FromImage(ResultImage);
        }
       public void Create()
        {           
            
        }
    }
}
