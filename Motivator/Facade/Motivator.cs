﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Motivator
{
    public class Motivator
    {
        public string CreateMotivator(string image, string message)
        {
            if (!File.Exists(image))
            {
                return $"Ошибка создания мотиватора. Файл с именем {image} не найден.";
            }

            string nameMotivator = "Motivator.jpg";           

            var backGround = new BackGround(1024, 768);            
            var frame = new Frame(20, backGround);
            var picture = new PictureWithBackGround(Image.FromFile(image), frame);
            var title = new Title(message, picture);
            title.Create();

            var motivator = title.ResultImage;
            motivator.Save(nameMotivator);

            return $"Создан мотиватор с именем: {nameMotivator}";
        }
    }
}
