﻿using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motivator
{
    class Program
    {
        static void Main()
        {            
            Console.Write("Введите номер картинки от 1 до 5: ");
            var fileImage = $"./Picture/{Console.ReadLine()}.jpg";           
            Console.Write("Введите текст мотиватора: ");
            var textMotivator = Console.ReadLine();

            var motivator = new Motivator();                
            Console.WriteLine(motivator.CreateMotivator(fileImage, textMotivator));
            
            Console.ReadLine();
        }
    }
}
